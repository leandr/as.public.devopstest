BACKEND_PORT    := 5000
TARGET          := Debug

-include .env

.EXPORT_ALL_VARIABLES:

.PHONY: help up build restore run test stop start down publish

help:
	echo "Usage:"
	echo "up - bring up docker-compose. create default configs"
	echo "stop - stop docker-compose"
	echo "restore - install deps"
	echo "build - build project"
	echo "pack - pack project"
	echo "publish - publish project"
	echo "test - test project"
	echo "run - start project"

up start: | .env docker/redis/data docker/nginx/conf.d/apdot_backend.conf
	docker-compose up --remove-orphans

.env:
	@echo ".env file was not found, creating with defaults"
	cp .env.dist .env

docker/nginx/sa_backend.conf:
	@echo "docker/nginx/conf.d/apdot_backend.conf file was not found, creating with defaults"
	cp docker/nginx/conf.d/apdot_backend.conf.dist docker/nginx/conf.d/apdot_backend.conf

docker/redis/data:
	mkdir -p docker/redis/data

stop down:
	docker-compose stop

restore:
	dotnet restore src/AS.Redis.Connector/AS.Redis.Connector.csproj

build:
	dotnet build --configuration "$(TARGET)" src/AS.Redis.Connector.sln

publish:
	dotnet publish --configuration "$(TARGET)" src/AS.Redis.Connector.sln

run:
	dotnet run --urls "http://0.0.0.0:$(BACKEND_PORT)" --project src/AS.Redis.Connector/AS.Redis.Connector.csproj

test:
	dotnet test src/AS.Redis.Connector.sln
