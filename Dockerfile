# build artifact

FROM mcr.microsoft.com/dotnet/sdk:3.1 AS builder

MAINTAINER "Leandr Khaliullov <leandr@cpan.org>"

WORKDIR /opt/AS.Public.DevopsTest

COPY . .

RUN dotnet test src/AS.Redis.Connector.sln

RUN dotnet publish --configuration Release src/AS.Redis.Connector/AS.Redis.Connector.csproj

# create image

FROM mcr.microsoft.com/dotnet/sdk:3.1

COPY --from=builder /opt/AS.Public.DevopsTest/src/AS.Redis.Connector/bin/Release/netcoreapp3.1/publish \
    /opt/AS.Public.DevopsTest

CMD dotnet /opt/AS.Public.DevopsTest/AS.Redis.Connector.dll
